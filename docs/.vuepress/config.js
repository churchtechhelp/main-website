module.exports = {
    title: 'Church Tech Help',
    description: 'Technical Help for Non-Technical Churches',
    //base: '/vuepress/',
    dest: 'public',
    head: [['script', {}, `
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="https://analytics.blackhawkelectronics.com/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '7']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
    `]],
    themeConfig: {
      nav: [
        { text: 'About', link: '/about' },
        { text: 'Training', link: '/training/' },
        { text: 'Helpful Resources', link: '/resources/' },
        { text: 'Blog', link: '/posts/' },
      ]
    },
    plugins: [
      ['@vuepress/blog',{
        directories: [
          {
            // Unique ID of current classification
            id: 'post',
            // Target directory
            dirname: '_posts',
            // Path of the `entry page` (or `list page`)
            path: '/posts/',
            pagination: {
              lengthPerPage: 10,
            }
          },
        ],
        sitemap: {
          hostname: 'https://churchtech.help',
        },
        feed: {
         canonical_base: 'https://churchtech.help',
        },
        frontmatters: [
          {
            // Unique ID of current classification
            id: 'tag',
            // Decide that the frontmatter keys will be grouped under this classification
            keys: ['tag'],
            // Path of the `entry page` (or `list page`)
            path: '/tag/',
            // Layout of the `entry page`
            layout: 'Tags',
            // Layout of the `scope page`
            scopeLayout: 'Tag'
          },
        ],
      }], // End of Vuepress Blog config
    ],
}
