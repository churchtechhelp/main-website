---
title: Doctrinal Statement
---

# Doctrinal Statement

All members should believe similarly.

- **Bibliology**: The Bible is the verbal, plenary preserved Word of God.
- **Anthropology**: God created the universe and all that is in it in 6 literal days.
- **Theology**: Jesus is the Son of God who lived, died, and rose again to pay for our sins.
- **Soteriology**: Salvation is by faith and not of works and is not forced.

# Standards
- Music should be "conservative"