---
title: About Church Tech Help
---

# About Us

Church Tech Help was started in 2020 by Benjamin Plain after
seeing a need for more authoritative knowledge about
technical resources at *conservative* churches.
He found that the information that was available was either focused
towards contemporary styles (e.g. drums, guitars, etc.)
or was only known by a few individuals, and
often tainted by bad experiences or faulty conclusions.

## Mission

Our **mission** is to promote the cause of Christ by providing
churches and ministries with the knowledge and understanding
of technology so that they are enabled to better achieve
their purpose.

## Goals

  - To instill a desire for spiritual growth through encouraging blog posts
  - To provide technical courses
  - To promote physical and legal safety
  - To enable the unseen organs of the body so they may enable the rest of the body (1 Cor. 12:12)  
    Many times, church members are uncomfortable volunteering for the media ministry because 
    they do not know what is expected of them or how to meet those expectations. 
  - To establish a standard of excellence for church technology

## Running Costs

We want to openly show how we plan to fund this ministry.

1. In the beginning stage, we will try to use free services.
2. Once established as a non-profit, we will start accepting donations.
3. Once we get training done, certifications will provide more income.

Once sufficient income is established to maintain Church Tech Help as a non-profit organization, we will pay our employees.
This will enable our employees to focus more on research, materials, and providing services to churches.
Currently all employees are volunteers and work at other jobs.

Any excess income will be saved and used for helping small churches
with technology.
