---
title: Professional Media Computer Setup
tag: Excelence, Professional, Media
---

# Setup your Media Computer so that it looks Professional

This post is about how I setup media computers so that they have a professional look.

These instructions are for Windows only.
You can also get specific software to play videos, PowerPoints, etc.

## The Hardware

  - Projector (Epson highly suggested due to superior 3LCD technology over 1DLP)
  - Media Computer
    - Windows 10
    - i5 or Ryzen 5 processor or better
    - 8GB of memory
    - SSD with at least 256GB
    - Must have at least 2 HDMI or DP outputs
    - Dedicated graphics card optional
  - 2 Monitors (FHD)
  - 1 1x2 HDMI splitter
  - 1 1x3 or 1x4 HDMI spitter (Kanex Pro suggested)
  - 1 Tiny screen in pulpit, back projector, or back big-screen TV

## The Software

**VLC** - VLC can play just about anything you give it, as long as it is a valid audio or video file.
Get the latest 64-bit version.

**Microsoft Office** - For PowerPoint presentations. Office can be purchased at a minimal cost at TechSoup if your church is a registered non-profit.

## The Setup

### User Setup

It is suggested that you either use an auto-login user or use a domain and do separate users. If you use a domain, you will need to do this setup for every new user on the computer.

1. Open Settings App
1. Go to Personalization
1. Set background to solid color and then black
1. To to Task bar page
1. Scroll down to *Multiple displays* section and turn off *Show taskbar on all displays*
1. You can also hide the task bar if you want. I find it useful for keeping it up on the main display.

### VLC Setup

The first time you open VLC, you will get asked if you want VLC to lookup media or check for updates.
I usually turn these two off to avoid any issues during production time.

Now we need to get rid of the cone and all the other on-screen stuff.

1. Open Preferences - Tools Menu -> Preferences
1. In the bottom left, click Show all settings radio button
1. Go to Interfaces -> Main Interfaces
1. Select Qt as the *Interface Module*
1. Go to Interfaces -> Main Interfaces -> Qt
1. Change the following settings:
   - *Show notification popup on track change* = Never
   - *Show a controller in fullscreen mode* = Unchecked
   - *Show unimportant error and warnings dialogs* - Unchecked
   - *Display background cone or art* = Unchecked
1. Go to Video -> Subtitles/OSD
1. Disable *On Screen Display*

### Office Setup

The biggest thing is to make sure the screens are setup right.
If you are on extend mode, make sure presenter view shows up on the right screen.
On modern versions, you can configure this in the *Slide Show* Tab in the *Monitors* section.
