---
title: Training and Certification
---

# Training and Certification

Welcome to the Church Tech Help training program!

## Sound

Most sound technicians figure out how to operate and modify a sound system by experience.
This often can result in bad habits that compromise sound quality.
The Church Tech Help Sound Technician courses are arranged in a three tier structure
to best meet the needs of churches. We realize that there are varing abilities of 
sound technicians at churches.

1. [Fundamentals of Sound](sound/Fundamentals.html) - For all technicians
1. [Advanced Sound](sound/Advanced.html) - For regular, trusted technicians
1. [Mastering Sound Systems](sound/Master.html) - For the main techncian

## Video

While sound can get by with experience alone, good videography requires knowledge.
Our videography courses go over the neccessary knowledge to get a visually pleasing live stream.

1. [Fundamentals of Videography](video/Fundamentals.html)
1. [Advanced Videography](video/Advanced.html)
1. [Video Editing](video/Editing.html)

## Lighting

Good video requires good lighting and beyond good lighting, a church also needs safe lighting.
The CTH lighting courses will teach you how to safely and properly using lighting in a church environment.

1. [Church Lighting Fundamentals](lighting/Fundamentals.html)
1. [Advanced Church Lighting](lighting/Advanced.md)

## Church Productions

Dramatic Productions are a culmination of Audio, Video, and Lighting technology, plus a lot of talent.

1. Script Design - Save money by making your own scripts
1. [Stagecraft](productions/stagecraft.html) - How to build a set
