---
title: Church Lighting Fundamentals
---

# Church Lighting Fundamentals

Good video requires good lighting.
Beyond good video, a church also needs safe lighting.
This course on lighting covers the majority of knowledge
on how to safely and properly setup lighting.

The course is focused more on knowing content more than understanding the content.
Course content should only be applied to existing lighting systems.
Understanding the content and designing new systems are a part of the advanced course.

## Learning Outcomes

  - Know proper safety procedures for lighting
  - Know the psycological effect of colors
  - Know the basics of electricity
  - Know the types of lighting equipment
  
