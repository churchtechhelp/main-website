---
title: Advanced Videography
---

# Advanced Videography

The intermidiate video course adds on to the beginner one, including
more advanced switching techniques to provide a more engaging
stream, and how to setup a video streaming system.

Learning Outcomes:
  - Understand how to do dynamic shots
  - Understand video layering
  - Understand the equipment needed for a full live-stream setup
  - Know how to design a live-stream setup

