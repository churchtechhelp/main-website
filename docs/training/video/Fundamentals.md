---
title: Fundamentals of Videography
---

# Fundamentals of Videography

The beginner video course is designed for those who are just
starting out on streaming, whether you are the camera man or 
are the one switching between cameras.

This course uses our simulator, so no equipment is needed.

Learning Outcomes:
  - Understand proper framing
  - Understand the basics of a camera
  - Know standard video terminology
  - Understands the concept of video switching

This course's capstone project is a live stream recording.
