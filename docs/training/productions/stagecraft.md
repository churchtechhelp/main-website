---
title: Stagecraft
---

# Stagecraft

Our course on stagecraft will enable volunteers to create
safe, reliable sets that follow standard dramatic production design.

The capstone project for this course is designing and building a set for a church play.
