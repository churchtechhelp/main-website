---
title: Advanced Sound
---

# Advanced Sound

The *Advanced Sound* course targets those who want a better understanding
of how to configure an existing sound system.
A focus on frequency and frequency manipulation allows the technician
to properly equalize inputs and outputs and understand how room acoustics affect sound
quality and clarity.

## Learning Outcomes

  - Understand gain structure
  - Understand equalization
  - Understand effects and when to use them
  - Understand digital routing
  - Know how to properly place microphones for productions
  - Know the various types of digital sound networks
  - Is able to identify areas that a sound system can be improved

## Certification

This course is a requirement for the Advanced Sound Technician Certification.
1. Completed the Novice Sound Technician certification
1. Completed all Advanced Sound content
1. Logged 20 more hours after completing the content
1. Passed the Advanced Sound Technician exam
