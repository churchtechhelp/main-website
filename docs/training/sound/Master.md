---
title: Mastering Sound
---

# Mastering Sound

The *Mastering Sound* course is for those who what to be able to design
and build a sound system from scratch, or redesign an existing sound system.

## Learning Outcomes

  - Understands all concepts in the Fundamental and Advanced sound courses
  - Able to design a new sound system following all best practices

## Certification

This course is a requirement for the Master Sound Technician

1. Complete all *Mastering Sound* course content
1. Complete a fully documented recomendation for a new sound system design
1. Have the recommendation approved by an instructor
1. Pass the Master Sound Technician Exam
