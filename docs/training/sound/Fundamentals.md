---
title: Fundamentals of Sound
---

# Fundamentals of Sound

The _Fundamentals of Sound_ course will teach a sound technician all that he needs to begin to understand what sound is.
This course will train individuals how to properly run an existing sound system.
This course will be as simple as technically possible.

## Learning Outcomes

  - An understanding of the basic components of a sound system
  - A basic understanding of what sound is and how it works
  - An understanding of proper mixing techniques

## Certification

This course is a requirement for the Novice Sound Technician Certification.

In order to get the certification you must complete _in order_:
1. Finished all content in *Fundamentals of Sound*
1. Acknowledgement by trainer that you are able to use your church's sound system without direct supervision
1. Have logged 20 solo hours on your church's sound system
1. Passed the Novice Sound Technician exam
