---
title: Book List
---

# Book List

The following is a list of books that are recommended

## General Books

None Yet

## Specialized Books

  - Videography
    - None yet
  - Lighting
    - _Stage Rigging Handbook_ by Jay O. Glerum  
      Know how to design **safe** rigging for your lights
  - Sound
    - _Master Handbook of Acoustics_ by F. ALton Everest and Ken C. Pohlmann  
      Goes over Acoustics in general, including psycoacoustics.
  - Stagecraft
    - None Yet

Know of a book that would be beneficial to others? Open an issue request on [GitLab](https://gitlab.com/churchtechhelp/main-website).
