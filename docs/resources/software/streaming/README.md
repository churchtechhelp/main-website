---
title: Streaming Software
---

# Streaming Software and Hardware

### Things to look for

1. Streams
1. Recording
1. Simulcasting (Streaming to multiple places at once)
1. Remote guests - useful for live missionary interviews or if church is closed
1. Layers
1. Ease of use
1. Cost

## The List

  - [Open Broadcasting Studio (Free)](streaming/obs.html)
  - [vMix](streaming/vmix.html)
  - [LiveStream Studio](streaming/livestream.html)
  - [Wirecast](streaming/wirecast.html)
