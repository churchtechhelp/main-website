---
title: OBS
---

# Open Broadcasting Studio

There are two variants of OBS - Regular OBS and StreamLabs OBS.
Streamlabs OBS is more user friendly but does mostly the same thing.

**Cost:** Free  
**Streams:** 1 stream  
**Recording:** 1 channel

## Setup

1. [Download](https://obsproject.com/download) and Install
1. Setup Output
1. Setup scenes
   - Keep in mind that all layers must have unique names.
   - If using Streaming Dashboad, keep the scene names short
