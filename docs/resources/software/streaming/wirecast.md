---
title: Wirecast
---

# Telestream Wirecast

**Cost:** $599  
**Streams:** Unlimited  
**Recording:** Unlimited  
**Remote Guests:** 2-7
**Simulcast:** Multiple Streams