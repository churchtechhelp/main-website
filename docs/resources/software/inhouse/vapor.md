---
title: Vapor DGS
---

# Vapor DGS

<img src="/logo-long.svg" style="height:10rem;" title="Vapor DGS Logo" />

> Whereas ye know not what shall be on the morrow. For what is your life?
> It is even a **vapour**, that appeareth for a little time, and then vanisheth away.  
> James 4:14 KJV

Vapor DGS stands for Visual Arts Portable Digital Graphics System.

## Features:

  - No special software to install. Just need Chrome.
  - Interactive displays
  - Supported Content: Video, Live stream, Images, Websites
  - Combine all of them
  - Inventory for equipment that is loaned/rented out
  - Non-live content is **offline** after being loaded, so you can take your display anywhere
  - Runs on *any* operating system, even your phone or tablet (web-app installation required).

### Future Add-Ons that have been suggested

  - Control external devices, like an Arduino. We are waiting on Google to release a serial interface to avoid installing software.

## Recomended Client Device Specs

Device Requirements change depending on what you have in your show.
Keep in mind that the more content that the device has to make (like animations), the more processing power you will need.

Chrome uses normal storage to store show files, so make sure you have a big enough drive to hold all content.
The client will not work if you do not have enough storage space.

### Windows Computers
If the device has to generate a lot of content, like for carousels or websites, then it is best to at least get an i3 or equivilent processor.
If you are only displaying a video, you can use a celeron or pentium.

### Raspberry Pi

The Raspberry pi is a very low-cost computer that runs Linux. The Raspberry Pi 4 even has 4k outputs on it.

The client has not yet been tested on a Raspberry Pi.

We will eventually make an image that only has the minimum requirements to get you going quickly.

### Android Tablet

Not yet tested.

### iPad

Not yet tested. Don't have one to test with.

## Setup

The most basic setup is opening up chrome and then going into full screen mode using F11.

The next step up would be to open Chrome in kiosk mode.

The best setup is to setup the computer so that it only goes into Chrome and never uses the normal desktop environment.
