---
title: Streaming Dashboard
---

# Streaming Dashboard

When you run many different pieces of hardware and software, 
controlling all of it can become unwieldy
and difficult to those with no training.

Church Tech Help's Streaming dashbard seeks to combine the control of many different parts
into a single page.

You can see Streaming Dashboard [here](http://streamer.churchtech.help).

View the project on [GitLab](https://gitlab.com/churchtechhelp/streaming-dashboard/web).

## Features

  - Make as many hotkeys as you want - any button can be controlled from the keyboard
  - Simulator (currently only has a few pictures)

## Currently Supported Hardware/Software

  - OBS (via OBS-Websocket)
  - Blackmagic ATEM switchers (via our ATEM relay) - multiple switchers supported

## Future Hardware/Software

  - Powerpoint
  - Keynote
  - VLC
  - BMD Hyperdeck
  - ATEM Mini Pro Recording and Streaming

## Contributing

We are looking for people to contribute!

If you have hardware, you can help us test.

If you know javascript, you can help us add new features.

If you have ideas, you can open issues on GitLab.
