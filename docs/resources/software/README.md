---
title: Software
---

# Software

To best help churches, we want to provide a list of curated software.
Some will include reviews and how-to's.

## Church Tech Help Software

 - [Streaming Dashboard](inhouse/streamingdashboard.html) - the *one* place to control your setup
 - [Vapor](inhouse/vapor.html) - Digital Graphics Display System

## External Software Topics

  - [Streaming Software](streaming/)
     - [Open Broadcasting Studio (Free)](streaming/obs.html)
     - [vMix](streaming/vmix.html)
     - [LiveStream Studio](streaming/livestream.html)
     - [Wirecast](streaming/wirecast.html)
