---
title: Resources
---

# Resources

[Book List](booklist.html)

[Software List](software/)

## Streaming Services

Recommended by us in **bold**.

  - **[SermonAudio](https://sermonaudio.com)**
  - [Resi](https://resi.io) - Formerly Living as One
  - Boxcast
  - TruthCasting
  - Vimeo LiveStream

## Other Technical Sites

The following sites cater to contemporary churches, but you may be able to get some information from them.
Church tech help is not affiliated with any of them.

  - [Behind the Mixer](https://www.behindthemixer.com/)
  - [Ministry Tech](https://ministrytech.com/)
  - [Learn Stage Lighting](https://learnstagelighting.com)
  - [Ministry Tech Magazine](https://ministrytech.com)
  - [Design and Tech Theatre](https://designandtechtheatre.wordpress.com/)
  - [Church Tech Today](https://churchtechtoday.com/)
  - [Church Production](https://www.churchproduction.com/)
