---
title: Church Security Resources
---

# Church Security Resources

## Training

CTH does not do security training.
The following are some resources that might help:

  - ???

## Security Plan

Every church should have a set of plans for every contigency.
It is best to talk to your local law enforcement when making these plans.

  - Active Shooter
  - Disruptive Individual
  - Disruptive Group
  - Rioters
  - Break-in (and how to avoid)
  - Medical emergency

## Hardware

**Radios**  
Radios are essential to quick and efficient communication. Cellphones will not do well in an emergency.

It is recommended to get a FCC Business License to ensure you legally will never have interference
and your communication will be private.
ISM Bands (900MHz, 2.4Ghz, 5GHz) are nice, but are prone to interference from other devices.
Registering multiple channels will also be helpful when you have multiple activities going on.
Security should always have their own dedicated channel.
