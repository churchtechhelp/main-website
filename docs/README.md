---
home: true
heroImage: /CTH-Test.png
heroText: Church Tech Help
features:
- title: Training
  details: Courses to improve your knowledge of church technology
- title: Resources
  details: Curated lists of software and hardware to help you pick the right tools
- title: Software
  details: Software to make tech easier
footer: GPLv3 Licensed | Copyright © 2020-present Benjamin Plain
---

Welcome to Church Tech Help!

We hope that the resources that you find here will improve your church's technology
to enable your church to do more.

See our page on [Facebook](https://fb.me/ChurchTechnologyHelp)!
